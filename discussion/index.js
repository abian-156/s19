//alert('hi');

// Exponent Operator

	// before
	const secondNum = Math.pow(8, 2)
	console.log(secondNum);

	// ES6 update
	const firstNum = 8 ** 2;
	console.log(firstNum);
//Template Literals
// Concatenation Operatro(+)

//ES6 Updates
	let message = 'Welcome to Programming'
	let name = 'John'
	console.log(message + " " + name);

	//expected result- Hello John, welcome
	//to programming.

	//ES6 updates
	console.log(`Hello ${name}, ${message}`);

/*Literals

[]- array literals

``- template literals

${}- placeholder

{}- object literals

*/

//Array destructuring
	// traditional Array Syntax- let arrayName = []
	// syntax
	/*
	 let [] = arrayName
*/

// Traditional array

const fullName = ['Jonathan', 'Joe', 'Star'];

// accessing array tradionally
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello, I am ${fullName[0]} ${fullName[1]}${fullName[2]}!`);

// array destructuring Es6 updates

const [firstName, middleName, lastName] = fullName;
console.log(`Hello, I am ${firstName} ${middleName} ${lastName}!`);

//OBJECT DESTRUCTURING

/*
syntax:
let/const {propertyName, propertyName,
propertyName} = object;
*/

const person = {
	givenName: 'Izuku',
	nickName: 'Deku',
	familyName: 'Midoriya'
};

//accessing object without destructing
console.log(person.givenName);
console.log(person.nickName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.nickName} ${person.lastName}! It's good to see you again.`)

/*//object destructuring

const {givenName, nickName, familyName} = person;
console.log(`Hello ${person.givenName} "${person.nickName}" ${person.familyName}! It's good to see you again.`)

function getFullName({givenName, nickName, familyName})
console.log(`Hello ${person.givenName} "${person.nickName}" ${person.familyName}! It's good to see you again.`)

getFullName(person);*/


//arrow Functions
//traditional function
//structure- conts functionName = (paramerter)

//=> {code block}

//function hello(parameter){
//	console.log('hello world')
// }

const hello = (parameter) => {
	console.log('Hello World')
}
hello();

const printFullName = (fName, mName, lName) => {
console.log(`${fName} ${mName} ${lName}`)
}

printFullName('Satoru', 'G', 'Gojo');

// arrw function in forEach

let students = ['megumi', 'nobara', 'yuji']

//traditional function
students.forEach(function(students){
	console.log(`${students} is a students`)
})

//arrow function
students.forEach((students) => {
	console.log(`${students} is a students`)
})

//implicit return statement

// const add = (x, y) => {
// 	return x + y
// }

// let total = add(1, 2)
// console.log(total);

// implicit return keyword
const add = (x, y) => x + y
let total = add(1, 2);
console.log(total);

//default function arument value

const greet = (name = 'User') => {
	return  `Good morning ${name}`
}
console.log(greet());

//class based object
	//creating a class based object
/*
syntax:

class className {
	constructor(objectPropertyA,
	objectPropertyB){
	this.objectPropertyA =
	}
}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}
// creating an instance

const myCar = new Car();
console.log(myCar);

//inserting Values to teh object

myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;
console.log(myCar);	

//creating an intance from car class with

const myNewCar = new Car('toyota', 'vios', 2021);
console.log(myNewCar)